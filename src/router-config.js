import Home from './pages/home';
import Itinerary from './pages/itinerary'
import MyBookings from './pages/myBookings'

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/itinerary',
        name: 'itinerary',
        component: Itinerary
    },
    {
        path: '/myBookings',
        name: 'mybookings',
        component: MyBookings
    }
];
